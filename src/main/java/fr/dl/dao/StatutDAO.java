package fr.dl.dao;

import fr.dl.metier.Statut;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class StatutDAO {

    Logger logger = LogManager.getLogger();

    public ArrayList<Statut> getLike(Statut statutSearched) {

        ArrayList<Statut> listeStatuts = new ArrayList<Statut>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchstatut ('" + statutSearched.getLibelle() + "')";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Statut statut = new Statut(rs.getInt(1), rs.getString(2));
                listeStatuts.add(statut);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return listeStatuts;
    }

    public ArrayList<Statut> getAll() {

        ArrayList<Statut> listeStatuts = new ArrayList<Statut>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "select * from Statut";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Statut statut = new Statut(rs.getInt(1), rs.getString(2));
                listeStatuts.add(statut);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return listeStatuts;
    }

    public boolean create(Statut statutCreated) {

        if (statutCreated.getLibelle() == null)
            statutCreated.setLibelle("");

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call createstatut ('" + statutCreated.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + statutCreated.getLibelle() + " a été créé dans la table Statut.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Statut statutSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call updatestatut (" + statutSelected.getId() + ",'" + statutSelected.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement d'id = " + statutSelected.getId() + " a été modifié dans la table Statut.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }

        return true;
    }

    public boolean delete(Statut statutSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call deletestatut (" + statutSelected.getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + statutSelected.getLibelle() + " a été supprimé de la table Statut.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }

        return true;
    }

    public int getMaxId() {
        int maxId = 0;
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();
            String strCmd = "select max(id_statut) from statut";
            rs = stmt.executeQuery(strCmd);
            if (rs.next())
                maxId = rs.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return maxId;
    }
}
