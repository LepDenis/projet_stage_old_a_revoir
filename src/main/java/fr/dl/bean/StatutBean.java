package fr.dl.bean;

import fr.dl.dao.StatutDAO;
import fr.dl.metier.Statut;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class StatutBean implements Serializable {

    private ArrayList<Statut> statuts;
    private Statut statutSearched;
    private Statut statutSelected;
    private Statut statutCreated;

    @PostConstruct
    private void initialize() {
        statutSearched = new Statut();
        statutSelected = new Statut();
        statutCreated = new Statut();
        statuts = new StatutDAO().getAll();
    }

    public void research() {
        StatutDAO dao = new StatutDAO();
        statuts = dao.getLike(statutSearched);
    }

    public void create() {
        StatutDAO dao = new StatutDAO();
        int i = dao.getMaxId() + 1;
        if (dao.create(statutCreated)) {
            statutCreated.setId(i);
            statuts.add(statutCreated);
            statutCreated = new Statut(i + 1, "");
        }
    }

    public void update() {
        StatutDAO dao = new StatutDAO();
        if (statutSelected != null) {
            int i = statuts.indexOf(statutSelected);
            if (dao.update(statutSelected)) {
                statuts.remove(i);
                statuts.add(i,statutSelected);
            }
        }
    }

    public void delete() {
        StatutDAO dao = new StatutDAO();
        if (statutSelected != null && dao.delete(statutSelected))
            statuts.remove(statutSelected);
    }

    public ArrayList<Statut> getStatuts() {
        return statuts;
    }

    public void setStatuts(ArrayList<Statut> statuts) {
        this.statuts = statuts;
    }

    public Statut getStatutSearched() {
        return statutSearched;
    }

    public void setStatutSearched(Statut statutSearched) {
        this.statutSearched = statutSearched;
    }

    public Statut getStatutSelected() {
        return statutSelected;
    }

    public void setStatutSelected(Statut statutSelected) {
        this.statutSelected = statutSelected;
    }

    public Statut getStatutCreated() {
        return statutCreated;
    }

    public void setStatutCreated(Statut statutCreated) {
        this.statutCreated = statutCreated;
    }
}
