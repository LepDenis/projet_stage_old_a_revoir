package fr.dl.bean;

import fr.dl.metier.Charge;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class ChargeBean implements Serializable {

    private ArrayList<Charge> charges;
    private Charge chargeSearched;
    private Charge chargeSelected;
    private Charge chargeCreated;

    @PostConstruct
    private void initialize() {

    }

    public void research() {

    }

    public void create() {

    }

    public void update() {

    }

    public void delete() {

    }

    public ArrayList<Charge> getCharges() {
        return charges;
    }

    public void setCharges(ArrayList<Charge> charges) {
        this.charges = charges;
    }

    public Charge getChargeSearched() {
        return chargeSearched;
    }

    public void setChargeSearched(Charge chargeSearched) {
        this.chargeSearched = chargeSearched;
    }

    public Charge getChargeSelected() {
        return chargeSelected;
    }

    public void setChargeSelected(Charge chargeSelected) {
        this.chargeSelected = chargeSelected;
    }

    public Charge getChargeCreated() {
        return chargeCreated;
    }

    public void setChargeCreated(Charge chargeCreated) {
        this.chargeCreated = chargeCreated;
    }
}
