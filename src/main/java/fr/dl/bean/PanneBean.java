package fr.dl.bean;

import fr.dl.dao.PanneDAO;
import fr.dl.metier.Panne;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class PanneBean implements Serializable {

    private ArrayList<Panne> pannes;
    private Panne panneSearched;
    private Panne panneSelected;
    private Panne panneCreated;

    @PostConstruct
    private void initialize() {
        panneSearched = new Panne();
        panneSelected = new Panne();
        panneCreated = new Panne();
        pannes = new PanneDAO().getAll();
    }

    public void research() {
        PanneDAO dao = new PanneDAO();
        pannes = dao.getLike(panneSearched);
    }

    public void create() {
        PanneDAO dao = new PanneDAO();
        int i = dao.getMaxId() + 1;
        if (dao.create(panneCreated)) {
            panneCreated.setId(i);
            pannes.add(panneCreated);
            panneCreated = new Panne(i + 1, "");
        }
    }

    public void update() {
        PanneDAO dao = new PanneDAO();
        if (panneSelected != null) {
            int i = pannes.indexOf(panneSelected);
            if (dao.update(panneSelected)) {
                pannes.remove(i);
                pannes.add(i, panneSelected);
            }
        }
    }

    public void delete() {
        PanneDAO dao = new PanneDAO();
        if (panneSelected != null && dao.delete(panneSelected))
            pannes.remove(panneSelected);
    }

    public ArrayList<Panne> getPannes() {
        return pannes;
    }

    public void setPannes(ArrayList<Panne> pannes) {
        this.pannes = pannes;
    }

    public Panne getPanneSearched() {
        return panneSearched;
    }

    public void setPanneSearched(Panne panneSearched) {
        this.panneSearched = panneSearched;
    }

    public Panne getPanneSelected() {
        return panneSelected;
    }

    public void setPanneSelected(Panne panneSelected) {
        this.panneSelected = panneSelected;
    }

    public Panne getPanneCreated() {
        return panneCreated;
    }

    public void setPanneCreated(Panne panneCreated) {
        this.panneCreated = panneCreated;
    }
}
