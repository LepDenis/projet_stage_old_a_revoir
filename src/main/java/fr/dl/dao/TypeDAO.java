package fr.dl.dao;

import fr.dl.metier.Type;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TypeDAO {

    Logger logger = LogManager.getLogger();

    public ArrayList<Type> getLike(Type typeSearched) {

        ArrayList<Type> listeTypes = new ArrayList<Type>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchtype ('" + typeSearched.getLibelle() + "')";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Type type = new Type(rs.getInt(1), rs.getString(2));
                listeTypes.add(type);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listeTypes;
    }

    public ArrayList<Type> getAll() {

        ArrayList<Type> listeTypes = new ArrayList<Type>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "select * from Type";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Type type = new Type(rs.getInt(1), rs.getString(2));
                listeTypes.add(type);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listeTypes;
    }

    public boolean create(Type typeCreated) {

        if (typeCreated.getLibelle() == null)
            typeCreated.setLibelle("");

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call createtype ('" + typeCreated.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + typeCreated.getLibelle() + " a été créé dans la table Type.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Type typeSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call updatetype (" + typeSelected.getId() + ",'" + typeSelected.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement d'id = " + typeSelected.getId() + " a été modifié dans la table Type.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean delete(Type typeSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call deletetype (" + typeSelected.getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + typeSelected.getLibelle() + " a été supprimé de la table Type.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public int getMaxId() {
        int maxId = 0;
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();
            String strCmd = "select max(id_type) from type";
            rs = stmt.executeQuery(strCmd);
            if (rs.next())
                maxId = rs.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return maxId;
    }
}
