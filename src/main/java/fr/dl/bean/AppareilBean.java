package fr.dl.bean;

import fr.dl.dao.AppareilDAO;
import fr.dl.metier.Appareil;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class AppareilBean implements Serializable {

    private ArrayList<Appareil> appareils;
    private Appareil appareilSearched;
    private Appareil appareilSelected;
    private Appareil appareilCreated;

    @PostConstruct
    private void initialize() {
        appareilSearched = new Appareil();
        appareilSelected = new Appareil();
        appareilCreated = new Appareil();
        appareils = new AppareilDAO().getAll();
    }

    public void research() {
        AppareilDAO dao = new AppareilDAO();
        /*
        int idApp = appareilSearched.getId();
        System.out.println(idApp);
        String modele = appareilSearched.getModele();
        System.out.println(modele);
        int typeId = appareilSearched.getType().getId();
        System.out.println(typeId);
        String libelleType = appareilSearched.getType().getLibelle();
        System.out.println(libelleType);
        int idMarque = appareilSearched.getMarque().getId();
        System.out.println(idMarque);
        String libelleMarque = appareilSearched.getMarque().getLibelle();
        System.out.println(libelleMarque);
        */
        appareils = dao.getLike(appareilSearched);
    }

    public void create() {
        AppareilDAO dao = new AppareilDAO();
        int i = dao.getMaxId() + 1;
        if (dao.create(appareilCreated)) {
            appareilCreated.setId(i);
            appareils.add(appareilCreated);
            appareilCreated = new Appareil(i + 1, "", null, null);
        }
    }

    public void update() {
        AppareilDAO dao = new AppareilDAO();
        if (appareilSelected != null) {
            int i = appareils.indexOf(appareilSelected);
            if (dao.update(appareilSelected)) {
                appareils.remove(i);
                appareils.add(i, appareilSelected);
            }
        }
    }

    public void delete() {
        AppareilDAO dao = new AppareilDAO();
        if (appareilSelected != null && dao.delete(appareilSelected)) {
            appareils.remove(appareilSelected);
        }
    }

    public ArrayList<Appareil> getAppareils() {
        return appareils;
    }

    public void setAppareils(ArrayList<Appareil> appareils) {
        this.appareils = appareils;
    }

    public Appareil getAppareilSearched() {
        return appareilSearched;
    }

    public void setAppareilSearched(Appareil appareilSearched) {
        this.appareilSearched = appareilSearched;
    }

    public Appareil getAppareilSelected() {
        return appareilSelected;
    }

    public void setAppareilSelected(Appareil appareilSelected) {
        this.appareilSelected = appareilSelected;
    }

    public Appareil getAppareilCreated() {
        return appareilCreated;
    }

    public void setAppareilCreated(Appareil appareilCreated) {
        this.appareilCreated = appareilCreated;
    }
}
