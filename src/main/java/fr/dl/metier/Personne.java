package fr.dl.metier;

public class Personne {

    private int id;
    private String nom;
    private String prenom;
    private String telephone;
    private String ligneAdresse1;
    private String ligneAdresse2;
    // qualite = client, fournisseur, technicien, etc...
    private Qualite qualite;
    private String adresseEmail;

    public Personne() {

    }

    public Personne(int id, String nom, String prenom, String telephone, String ligneAdresse1,
                    String ligneAdresse2, Qualite qualite, String adresseEmail) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.ligneAdresse1 = ligneAdresse1;
        this.ligneAdresse2 = ligneAdresse2;
        this.qualite = qualite;
        this.adresseEmail = adresseEmail;
    }

    public Personne(int id, String nom, String prenom, String telephone, String ligneAdresse1, String ligneAdresse2, String adresseEmail){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.ligneAdresse1 = ligneAdresse1;
        this.ligneAdresse2 = ligneAdresse2;
        this.adresseEmail = adresseEmail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLigneAdresse1() {
        return ligneAdresse1;
    }

    public void setLigneAdresse1(String ligneAdresse1) {
        this.ligneAdresse1 = ligneAdresse1;
    }

    public String getLigneAdresse2() {
        return ligneAdresse2;
    }

    public void setLigneAdresse2(String ligneAdresse2) {
        this.ligneAdresse2 = ligneAdresse2;
    }

    public Qualite getQualite() {
        return qualite;
    }

    public void setQualite(Qualite qualite) {
        this.qualite = qualite;
    }

    public String getAdresseEmail() {
        return adresseEmail;
    }

    public void setAdresseEmail(String adresseEmail) {
        this.adresseEmail = adresseEmail;
    }

    @Override
    public String toString() {
        return id + "  " + nom + "  " + prenom + "  " + telephone + "  " + ligneAdresse1 + "  " + ligneAdresse2 + "  " + adresseEmail;
    }

}
