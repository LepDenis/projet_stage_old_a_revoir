package fr.dl.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connect {
    // Declare the JDBC objects.
    private Connection connection;
    private static Connect instance;
    private Logger logger = LogManager.getLogger();

    private Connect() {
        /* Chargement du driver JDBC pour MySQL */
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            logger.error("Erreur driver: " + e.getMessage());
        }
        try {
            String url = "jdbc:mysql://localhost:3308/pcclinic?autoReconnect=true&useSSL=false";
            String utilisateur = "root";
            // String motDePasse = "root";

            connection = DriverManager.getConnection(url, utilisateur, "");
        }
        // Handle any errors that may have occurred.
        catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
    public static Connect getInstance() {
        if (instance == null) instance = new Connect();
        return instance;
    }
    public Connection getConnection() {
        return connection;
    }
}
