/*
delimiter $$

create procedure createpersonne(in _nom text, in _prenom text, in _tel text, in _adresse text, 
in _adresse2 text, in _idqualite int, in _adresseemail text)

BEGIN

DECLARE rb bool default 0 ; 
declare maxid int ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;
set maxid = (select max(id_personne) from personne);

if maxid is null then
set maxid = 0;
end if;

insert into personne (id_personne, nom, prenom, tel, adresse, id_qualite, adresse2, adresse_email) 
values ((maxid + 1), _nom, _prenom, _tel, _adresse, _idqualite, _adresse2, _adresseemail);

if rb then
	rollback;
else
	commit;
end if;

END$$

delimiter ;
*/

/*
delimiter $$

create procedure updatepersonne(in _idpersonne int, in _nom text, in _prenom text, in _tel text, 
in _adresse text, in _adresse2 text, in _idqualite int, in _adresseemail text)

begin

declare rb bool default 0 ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;

update personne

set nom = IF(_nom not like '', _nom, nom),
 prenom = IF(_prenom not like '', _prenom, prenom),
 tel = IF(_tel not like '', _tel, tel),
 adresse = IF(_adresse not like '', _adresse, adresse),
 adresse2 = IF(_adresse2 not like '', _adresse2, adresse2),
 id_qualite = IF(_idqualite > 0, _idqualite, id_qualite),
 adresse_email = IF(_adresseemail not like '', _adresseemail, adresse_email)
where id_personne = _idpersonne;

if rb = 1 then
rollback;
else
commit;
end if;

END $$

delimiter ;
*/

/*
delimiter $$

create procedure deletepersonne (in _idpersonne int)

begin

declare rb bool default 0;
declare continue handler for sqlexception set rb = 1;

start transaction;

delete from personne where id_personne = _idpersonne;

if rb = 1 then
rollback;
else
commit;
end if;

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchpersonne (in _nom text, in _prenom text, in _tel text, in _adresse text, 
in _adresse2 text, in _idqualite int, in _adresseemail text)

begin

select id_personne, nom, prenom, tel, adresse, adresse2, id_qualite, adresse_email from personne where
nom like concat('%', _nom, '%') and 
prenom like concat('%', _prenom, '%') and 
tel like concat('%', _tel, '%') and
adresse like concat('%', _adresse, '%') and 
adresse2 like concat('%', _adresse2, '%') and
id_qualite = _idqualite and
adresse_email like concat('%', _adresseemail, '%');

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchpersonnebyqualite (in _qualite int)

begin

select id_personne, nom, prenom, tel, adresse, adresse2, id_qualite, adresse_email from personne where id_qualite = _qualite;

end $$

delimiter ;
*/


/*
delimiter $$
create procedure QBEintervention (in _nomClient text, in _villeClient text, 
							in _nomTechnicien text, in _lieuIntervention int, 
							in _dateDebutBasse datetime, in _dateDebutHaute datetime, 
                            in _dateFinBasse datetime, in _dateFinHaute datetime,
							in _sommeDueBasse float, in _sommeDueHaute float,
							in _modeleAppareil text, in _libellePanne text, in _libelleStatut text)

begin

if _nomClient is null then set _nomClient = '';end if;

set @clauseSelect = 'select C.somme_due,
	I.id_intervention, I.lieu, I.debut, I.fin,
    Cl.id_personne, Cl.nom, Cl.prenom, Cl.tel, Cl.adresse, Cl.adresse2, Cl.adresse_email,
    Tc.id_personne, Tc.nom, Tc.prenom, Tc.tel, Tc.adresse, Tc.adresse2, Tc.adresse_email,
	A.id_appareil, A.modele, T.id_type, T.libelle, M.id_marque, M.libelle,  
    C.id_panne, 
	(select P.libelle from panne as P where P.id_panne = C.id_panne) as panne,
    C.id_statut, 
    (select S.libelle from statut as S where S.id_statut = C.id_statut) as statut
	from caracteriser as C

left join intervention as I on I.id_intervention = C.id_intervention
left join personne as Cl on Cl.id_personne = I.id_beneficiaire
left join personne as Tc on Tc.id_personne = I.id_intervenant
left join appareil as A on A.id_appareil = C.id_appareil
left join type as T on T.id_type = A.id_type
left join marque as M on M.id_marque = A.id_marque';

set @clauseWhere = concat('where Cl.nom like \'%',_nomClient,'%\'');
if (_villeClient != '') then set @clauseWhere = concat(@clauseWhere, 'and Cl.adresse2 like \'%',_villeClient,'%\' ');end if;
if (_nomTechnicien != '') then set @clauseWhere = concat(@clauseWhere,'and Tc.nom like \'%',_nomTechnicien,'%\' ');end if;
if (_lieuIntervention between 1 and 2) then set @clauseWhere = concat(@clauseWhere,'and I.lieu = ',_lieuIntervention,' ');end if;
if (_dateDebutBasse is not null) then set @clauseWhere = concat(@clauseWhere,'and I.debut >= \'',_dateDebutBasse,'\' ');end if;
if (_dateDebutHaute is not null AND _dateDebutHaute >= _dateDebutBasse) then 
	set @clauseWhere = concat(@clauseWhere,'and I.debut <= \'',_dateDebutHaute,'\' ');end if;
if (_dateFinBasse is not null) then set @clauseWhere = concat(@clauseWhere,'and I.fin >= \'',_dateFinBasse,'\' ');end if;
if (_dateFinHaute is not null AND _dateFinHaute >= _dateFinBasse) then 
	set @clauseWhere = concat(@clauseWhere,'and I.fin <= \'',_dateFinHaute,'\' ');end if;
if (_sommeDueBasse >= 0) then set @clauseWhere = concat(@clauseWhere,'and C.somme_due >= ',_sommeDueBasse,' ');end if;
if (_sommeDueHaute >= 0 AND _sommeDueHaute > _sommeDueBasse) then 
	set @clauseWhere = concat(@clauseWhere,'and C.somme_due <= ',_sommeDueHaute,' ');end if;
if (_modeleAppareil != '') then set @clauseWhere = concat(@clauseWhere, 'and A.modele like \'%',_modeleAppareil,'%\' ');end if;
if (_libellePanne != '') then set @clauseWhere = concat(@clauseWhere, 'and P.libelle like \'%',_libellePanne,'%\' ');end if;
if (_libelleStatut != '') then set @clauseWhere = concat(@clauseWhere, 'and S.libelle like \'%',_libelleStatut,'%\' ');end if;

set @completeQuery = concat(@clauseSelect,' ',@clauseWhere,';');

prepare stmt from @completeQuery;
execute stmt;

end $$
delimiter ;
*/


/*
delimiter $$

create procedure createtype (in _libelle text)

BEGIN

DECLARE rb bool default 0 ; 
declare maxid int ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;
set maxid = (select max(id_type) from type);

if maxid is null then
set maxid = 0;
end if;

insert into type (id_type, libelle) values ((maxid + 1), _libelle);

if rb then
	rollback;
else
	commit;
end if;

END$$

delimiter ;
*/

/*
delimiter $$

create procedure updatetype (in _idtype int, in _libelle text)

begin

declare rb bool default 0 ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;

update type

set type = IF(_libelle not like '', _libelle, libelle)

where id_type = _idtype;

if rb = 1 then
rollback;
else
commit;
end if;

END $$

delimiter ;
*/

/*
delimiter $$

create procedure deletetype (in _idtype int)

begin

declare rb bool default 0;
declare continue handler for sqlexception set rb = 1;

start transaction;

delete from type where id_type = _idtype;

if rb = 1 then
rollback;
else
commit;
end if;

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchtype (in _libelle text)

begin

select id_type, libelle from type where
libelle like concat('%', _libelle, '%');

end $$

delimiter ;
*/


/*
delimiter $$

create procedure createstatut (in _libelle text)

BEGIN

DECLARE rb bool default 0 ; 
declare maxid int ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;
set maxid = (select max(id_statut) from statut);

if maxid is null then
set maxid = 0;
end if;

insert into statut (id_statut, libelle) values ((maxid + 1), _libelle);

if rb then
	rollback;
else
	commit;
end if;

END$$

delimiter ;
*/

/*
delimiter $$

create procedure updatestatut (in _idstatut int, in _libelle text)

begin

declare rb bool default 0 ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;

update statut

set marque = IF(_libelle not like '', _libelle, libelle)
where id_statut = _idstatut;

if rb = 1 then
rollback;
else
commit;
end if;

END $$

delimiter ;
*/

/*
delimiter $$

create procedure deletestatut (in _idstatut int)

begin

declare rb bool default 0;
declare continue handler for sqlexception set rb = 1;

start transaction;

delete from statut where id_statut = _idstatut;

if rb = 1 then
rollback;
else
commit;
end if;

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchstatut (in _libelle text)

begin

select id_statut, libelle from statut where
libelle like concat('%', _libelle, '%');

end $$

delimiter ;
*/


/*
delimiter $$

create procedure createqualite (in _libelle text)

BEGIN

DECLARE rb bool default 0 ; 
declare maxid int ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;
set maxid = (select max(id_qualite) from qualite);

if maxid is null then
set maxid = 0;
end if;

insert into qualite (id_qualite, libelle) values ((maxid + 1), _libelle);

if rb then
	rollback;
else
	commit;
end if;

END$$

delimiter ;
*/

/*
delimiter $$

create procedure updatequalite (in _idqualite int, in _libelle text)

begin

declare rb bool default 0 ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;

update qualite

set qualite = IF(_libelle not like '', _libelle, libelle)
where id_qualite = _idqualite;

if rb = 1 then
rollback;
else
commit;
end if;

END $$

delimiter ;
*/

/*
delimiter $$

create procedure deletequalite (in _idqualite int)

begin

declare rb bool default 0;
declare continue handler for sqlexception set rb = 1;

start transaction;

delete from qualite where id_qualite = _idqualite;

if rb = 1 then
rollback;
else
commit;
end if;

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchqualite (in _libelle text)

begin

select id_qualite, libelle from qualite where
libelle like concat('%', _libelle, '%');

end $$

delimiter ;
*/


/*
delimiter $$

create procedure createpanne (in _libelle text)

BEGIN

DECLARE rb bool default 0 ; 
declare maxid int ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;
set maxid = (select max(id_panne) from panne);

if maxid is null then
set maxid = 0;
end if;

insert into panne (id_panne, libelle) values ((maxid + 1), _libelle);

if rb then
	rollback;
else
	commit;
end if;

END$$

delimiter ;
*/

/*
delimiter $$

create procedure updatepanne (in _idpanne int, in _libelle text)

begin

declare rb bool default 0 ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;

update panne

set panne = IF(_libelle not like '', _libelle, libelle)
where id_panne = _idpanne;

if rb = 1 then
rollback;
else
commit;
end if;

END $$

delimiter ;
*/

/*
delimiter $$

create procedure deletepanne (in _idpanne int)

begin

declare rb bool default 0;
declare continue handler for sqlexception set rb = 1;

start transaction;

delete from panne where id_panne = _idpanne;

if rb = 1 then
rollback;
else
commit;
end if;

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchpanne (in _libelle text)

begin

select id_panne, libelle from panne where
libelle like concat('%', _libelle, '%');

end $$

delimiter ;
*/


/*
delimiter $$

create procedure createmarque (in _libelle text)

BEGIN

DECLARE rb bool default 0 ; 
declare maxid int ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;
set maxid = (select max(id_marque) from marque);

if maxid is null then
set maxid = 0;
end if;

insert into marque (id_marque, libelle) values ((maxid + 1), _libelle);

if rb then
	rollback;
else
	commit;
end if;

END$$

delimiter ;
*/

/*
delimiter $$

create procedure updatemarque (in _idmarque int, in _libelle text)

begin

declare rb bool default 0 ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;

update marque

set marque = IF(_libelle not like '', _libelle, libelle)
where id_marque = _idmarque;

if rb = 1 then
rollback;
else
commit;
end if;

END $$

delimiter ;
*/

/*
delimiter $$

create procedure deletemarque (in _idmarque int)

begin

declare rb bool default 0;
declare continue handler for sqlexception set rb = 1;

start transaction;

delete from marque where id_marque = _idmarque;

if rb = 1 then
rollback;
else
commit;
end if;

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchmarque (in _libelle text)

begin

select id_marque, libelle from marque where
libelle like concat('%', _libelle, '%');

end $$

delimiter ;
*/


/*
delimiter $$

create procedure createappareil(in _modele text, in _idtype int, in _idmarque int)

BEGIN

DECLARE rb bool default 0 ; 
declare maxid int ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;
set maxid = (select max(id_appareil) from appareil);

if maxid is null then
set maxid = 0;
end if;

insert into appareil (id_appareil, modele, id_type, id_marque) values ((maxid + 1), _modele, _idtype, _idmarque);

if rb then
	rollback;
else
	commit;
end if;

END$$

delimiter ;
*/

/*
delimiter $$

create procedure updateappareil(in _idappareil int, in _modele text, in _idtype int, in _idmarque int)

begin

declare rb bool default 0 ;
declare continue handler for sqlexception set rb = 1 ;

start transaction;

update appareil

set modele = IF(_modele not like '', _modele, modele),
 id_type = IF(_idtype > 0, _idtype, id_type),
 id_marque = IF(_idmarque > 0, _idmarque, id_marque)
where id_appareil = _idappareil;

if rb = 1 then
rollback;
else
commit;
end if;

END $$

delimiter ;
*/

/*
delimiter $$

create procedure deleteappareil (in _idappareil int)

begin

declare rb bool default 0;
declare continue handler for sqlexception set rb = 1;

start transaction;

delete from personne where id_appareil = _idappareil;

if rb = 1 then
rollback;
else
commit;
end if;

end $$

delimiter ;
*/

/*
delimiter $$

create procedure searchappareil (in _modele text, in _idtype int, in _idmarque int)

begin

if _modele is null then set _modele = '';end if;
if _idtype is null then set _idtype = 0;end if;
if _idmarque is null then set _idmarque = 0;end if;

set @clauseSelect = 'select A.id_appareil, A.modele, A.id_type,
(select T.libelle from type as T where A.id_type = T.id_type),
A.id_marque,
(select M.libelle from marque as M where A.id_marque = M.id_marque) 
from appareil as A';

set @clauseWhere = concat('where modele like \'%', _modele, '%\'');
if (_idtype > 0) then set @clauseWhere = concat(@clauseWhere, ' and id_type = ', _idtype, ' ');end if;
if (_idmarque > 0) then set @clauseWhere = concat(@clauseWhere, ' and id_marque = ', _idmarque, ' ');end if;

set @completeQuery = concat(@clauseSelect, ' ', @clauseWhere);
prepare stmt from @completeQuery;
execute stmt;

end $$

delimiter ;
*/


