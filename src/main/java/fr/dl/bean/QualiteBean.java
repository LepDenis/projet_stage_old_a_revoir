package fr.dl.bean;

import fr.dl.dao.QualiteDAO;
import fr.dl.metier.Qualite;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class QualiteBean implements Serializable {

    private ArrayList<Qualite> qualites;
    private Qualite qualiteSearched;
    private Qualite qualiteSelected;
    private Qualite qualiteCreated;

    @PostConstruct
    private void initialize() {
        qualiteSearched = new Qualite();
        qualiteSelected = new Qualite();
        qualiteCreated = new Qualite();
        qualites = new QualiteDAO().getAll();
    }

    public void research() {
        QualiteDAO dao = new QualiteDAO();
        qualites = dao.getLike(qualiteSearched);
    }

    public void create() {
        QualiteDAO dao = new QualiteDAO();
        int i = dao.getMaxId() + 1;
        if (dao.create(qualiteCreated)) {
            qualiteCreated.setId(i);
            qualites.add(qualiteCreated);
            qualiteCreated = new Qualite(i + 1, "");
        }
    }

    public void update() {
        QualiteDAO dao = new QualiteDAO();
        if (qualiteSelected != null) {
            int i = qualites.indexOf(qualiteSelected);
            if (dao.update(qualiteSelected)) {
                qualites.remove(i);
                qualites.add(i, qualiteSelected);
            }
        }
    }

    public void delete() {
        QualiteDAO dao = new QualiteDAO();
        if (qualiteSelected != null && dao.delete(qualiteSelected))
            qualites.remove(qualiteSelected);
    }

    public ArrayList<Qualite> getQualites() {
        return qualites;
    }

    public void setQualites(ArrayList<Qualite> qualites) {
        this.qualites = qualites;
    }

    public Qualite getQualiteSearched() {
        return qualiteSearched;
    }

    public void setQualiteSearched(Qualite qualiteSearched) {
        this.qualiteSearched = qualiteSearched;
    }

    public Qualite getQualiteSelected() {
        return qualiteSelected;
    }

    public void setQualiteSelected(Qualite qualiteSelected) {
        this.qualiteSelected = qualiteSelected;
    }

    public Qualite getQualiteCreated() {
        return qualiteCreated;
    }

    public void setQualiteCreated(Qualite qualiteCreated) {
        this.qualiteCreated = qualiteCreated;
    }
}
