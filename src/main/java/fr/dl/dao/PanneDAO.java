package fr.dl.dao;

import fr.dl.metier.Panne;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PanneDAO {

    private Logger logger = LogManager.getLogger();

    public ArrayList<Panne> getAll() {

        ArrayList<Panne> listePannes = new ArrayList<Panne>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "select * from Panne";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Panne panne = new Panne(rs.getInt(1), rs.getString(2));
                listePannes.add(panne);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return listePannes;
    }

    public ArrayList<Panne> getLike(Panne panneSearched) {

        ArrayList<Panne> listePannes = new ArrayList<Panne>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchpanne ('" + panneSearched.getLibelle() + "')";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Panne panne = new Panne(rs.getInt(1), rs.getString(2));
                listePannes.add(panne);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return listePannes;
    }

    public boolean create(Panne panneCreated) {

        if (panneCreated.getLibelle() == null)
            panneCreated.setLibelle("");

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call createpanne ('" + panneCreated.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + panneCreated.getLibelle() + " a été créé dans la table Panne.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Panne panneSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call updatepanne (" + panneSelected.getId() + ",'" + panneSelected.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement d'id = " + panneSelected.getId() + " a été modifié dans la table Panne.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean delete(Panne panneSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call deletepanne (" + panneSelected.getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + panneSelected.getLibelle() + " a été supprimé de la table Panne.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public int getMaxId() {
        int maxId = 0;
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();
            String strCmd = "select max(id_panne) from panne";
            rs = stmt.executeQuery(strCmd);
            if (rs.next())
                maxId = rs.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return maxId;
    }

}
