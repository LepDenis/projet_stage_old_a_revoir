#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------
USE pcclinic;

#------------------------------------------------------------
# Table: Personne
#------------------------------------------------------------

CREATE TABLE Personne(
        id_personne Int NOT NULL ,
        nom         Text NOT NULL ,
        prenom      Text ,
        tel         Text ,
        adresse     Text ,
        adresse2	Text ,
        id_qualite  Int NOT NULL ,	
	adresse_email Text,
        PRIMARY KEY (id_personne )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Intervention
#------------------------------------------------------------

CREATE TABLE Intervention(
        id_intervention      Int NOT NULL ,
        lieu                 Int NOT NULL ,
        debut                Date NOT NULL ,
        fin                  Date ,
        id_intervenant       Int NOT NULL ,
        id_beneficiaire      Int NOT NULL ,
        PRIMARY KEY (id_intervention )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Panne
#------------------------------------------------------------

CREATE TABLE Panne(
        id_panne Int NOT NULL ,
        libelle  Varchar(100) NOT NULL ,
        PRIMARY KEY (id_panne ) ,
        UNIQUE (libelle )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Appareil
#------------------------------------------------------------

CREATE TABLE Appareil(
        id_appareil Int NOT NULL ,
        modele      Varchar(100) NOT NULL ,
        id_type     Int NOT NULL ,
        id_marque   Int NOT NULL ,
        PRIMARY KEY (id_appareil ) ,
        UNIQUE (modele )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Type
#------------------------------------------------------------

CREATE TABLE Type(
        id_type Int NOT NULL ,
        libelle Varchar(100) NOT NULL ,
        PRIMARY KEY (id_type ) ,
        UNIQUE (libelle )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Marque
#------------------------------------------------------------

CREATE TABLE Marque(
        id_marque Int NOT NULL ,
        libelle   Varchar(100) NOT NULL ,
        PRIMARY KEY (id_marque ) ,
        UNIQUE (libelle )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Piece
#------------------------------------------------------------

CREATE TABLE Piece(
        id_piece        Int NOT NULL ,
        modele          Varchar(100) NOT NULL ,
        stock           Int ,
        cout            Float NOT NULL ,
        id_marque       Int NOT NULL ,
        id_type         Int NOT NULL ,
        id_piece_perdue Int NOT NULL ,
        PRIMARY KEY (id_piece ) ,
        UNIQUE (modele )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
        id_commande    Int NOT NULL ,
        date_commande  Date NOT NULL ,
        date_reception Date ,
        id_personne    Int NOT NULL ,
        PRIMARY KEY (id_commande )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Charge
#------------------------------------------------------------

CREATE TABLE Charge(
        id_charge     Int NOT NULL ,
        montant       Int NOT NULL ,
        date_charge   Date NOT NULL ,
        date_paiement Date ,
        id_detail     Int NOT NULL ,
        PRIMARY KEY (id_charge )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Detail
#------------------------------------------------------------

CREATE TABLE Detail(
        id_detail   Int NOT NULL ,
        libelle     Varchar(100) NOT NULL ,
        variabilite Int NOT NULL ,
        PRIMARY KEY (id_detail ) ,
        UNIQUE (libelle )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Compte
#------------------------------------------------------------

CREATE TABLE Compte(
        id_compte    Int NOT NULL ,
        login        Varchar(100) NOT NULL ,
        hash_mdp     Text NOT NULL ,
        autorisation Int NOT NULL ,
        id_personne  Int NOT NULL ,
        PRIMARY KEY (id_compte ) ,
        UNIQUE (login )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Statut
#------------------------------------------------------------

CREATE TABLE Statut(
        id_statut Int NOT NULL ,
        libelle   Varchar(100) NOT NULL ,
        PRIMARY KEY (id_statut ) ,
        UNIQUE (libelle )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Qualite
#------------------------------------------------------------

CREATE TABLE Qualite(
        id_qualite Int NOT NULL ,
        libelle    Varchar(100) NOT NULL ,
        PRIMARY KEY (id_qualite ) ,
        UNIQUE (libelle )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Perte
#------------------------------------------------------------

CREATE TABLE Perte(
        id_piece_perdue Int NOT NULL ,
        modele          Text ,
        cout            Int NOT NULL ,
        id_piece        Int NOT NULL ,
        PRIMARY KEY (id_piece_perdue )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Caracteriser
#------------------------------------------------------------

CREATE TABLE Caracteriser(
        somme_due       Float NOT NULL ,
        id_intervention Int NOT NULL ,
        id_panne        Int NOT NULL ,
        id_statut       Int NOT NULL ,
        id_appareil     Int NOT NULL ,
        PRIMARY KEY (id_intervention ,id_panne ,id_statut ,id_appareil )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Peut necessiter
#------------------------------------------------------------

CREATE TABLE Peut_necessiter(
        id_panne Int NOT NULL ,
        id_piece Int NOT NULL ,
        PRIMARY KEY (id_panne ,id_piece )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Posseder
#------------------------------------------------------------

CREATE TABLE Posseder(
        id_appareil Int NOT NULL ,
        id_personne Int NOT NULL ,
        PRIMARY KEY (id_appareil ,id_personne )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Etre composee
#------------------------------------------------------------

CREATE TABLE Etre_composee(
        quantite    Int NOT NULL ,
        id_piece    Int NOT NULL ,
        id_commande Int NOT NULL ,
        PRIMARY KEY (id_piece ,id_commande )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Peut induire
#------------------------------------------------------------

CREATE TABLE Peut_induire(
        id_personne Int NOT NULL ,
        id_charge   Int NOT NULL ,
        PRIMARY KEY (id_personne ,id_charge )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Etre causee
#------------------------------------------------------------

CREATE TABLE Etre_causee(
        id_charge   Int NOT NULL ,
        id_commande Int NOT NULL ,
        PRIMARY KEY (id_charge ,id_commande )
)ENGINE=InnoDB;

ALTER TABLE Personne ADD CONSTRAINT FK_Personne_id_qualite FOREIGN KEY (id_qualite) REFERENCES Qualite(id_qualite);
ALTER TABLE Intervention ADD CONSTRAINT FK_Intervention_id_intervenant FOREIGN KEY (id_intervenant) REFERENCES Personne(id_personne);
ALTER TABLE Intervention ADD CONSTRAINT FK_Intervention_id_beneficiaire FOREIGN KEY (id_beneficiaire) REFERENCES Personne(id_personne);
ALTER TABLE Appareil ADD CONSTRAINT FK_Appareil_id_type FOREIGN KEY (id_type) REFERENCES Type(id_type);
ALTER TABLE Appareil ADD CONSTRAINT FK_Appareil_id_marque FOREIGN KEY (id_marque) REFERENCES Marque(id_marque);
ALTER TABLE Piece ADD CONSTRAINT FK_Piece_id_marque FOREIGN KEY (id_marque) REFERENCES Marque(id_marque);
ALTER TABLE Piece ADD CONSTRAINT FK_Piece_id_type FOREIGN KEY (id_type) REFERENCES Type(id_type);
ALTER TABLE Piece ADD CONSTRAINT FK_Piece_id_piece_perdue FOREIGN KEY (id_piece_perdue) REFERENCES Perte(id_piece_perdue);
ALTER TABLE Commande ADD CONSTRAINT FK_Commande_id_personne FOREIGN KEY (id_personne) REFERENCES Personne(id_personne);
ALTER TABLE Charge ADD CONSTRAINT FK_Charge_id_detail FOREIGN KEY (id_detail) REFERENCES Detail(id_detail);
ALTER TABLE Compte ADD CONSTRAINT FK_Compte_id_personne FOREIGN KEY (id_personne) REFERENCES Personne(id_personne);
ALTER TABLE Perte ADD CONSTRAINT FK_Perte_id_piece FOREIGN KEY (id_piece) REFERENCES Piece(id_piece);
ALTER TABLE Caracteriser ADD CONSTRAINT FK_Caracteriser_id_intervention FOREIGN KEY (id_intervention) REFERENCES Intervention(id_intervention);
ALTER TABLE Caracteriser ADD CONSTRAINT FK_Caracteriser_id_panne FOREIGN KEY (id_panne) REFERENCES Panne(id_panne);
ALTER TABLE Caracteriser ADD CONSTRAINT FK_Caracteriser_id_statut FOREIGN KEY (id_statut) REFERENCES Statut(id_statut);
ALTER TABLE Caracteriser ADD CONSTRAINT FK_Caracteriser_id_appareil FOREIGN KEY (id_appareil) REFERENCES Appareil(id_appareil);
ALTER TABLE Peut_necessiter ADD CONSTRAINT FK_Peut_necessiter_id_panne FOREIGN KEY (id_panne) REFERENCES Panne(id_panne);
ALTER TABLE Peut_necessiter ADD CONSTRAINT FK_Peut_necessiter_id_piece FOREIGN KEY (id_piece) REFERENCES Piece(id_piece);
ALTER TABLE Posseder ADD CONSTRAINT FK_Posseder_id_appareil FOREIGN KEY (id_appareil) REFERENCES Appareil(id_appareil);
ALTER TABLE Posseder ADD CONSTRAINT FK_Posseder_id_personne FOREIGN KEY (id_personne) REFERENCES Personne(id_personne);
ALTER TABLE Etre_composee ADD CONSTRAINT FK_Etre_composee_id_piece FOREIGN KEY (id_piece) REFERENCES Piece(id_piece);
ALTER TABLE Etre_composee ADD CONSTRAINT FK_Etre_composee_id_commande FOREIGN KEY (id_commande) REFERENCES Commande(id_commande);
ALTER TABLE Peut_induire ADD CONSTRAINT FK_Peut_induire_id_personne FOREIGN KEY (id_personne) REFERENCES Personne(id_personne);
ALTER TABLE Peut_induire ADD CONSTRAINT FK_Peut_induire_id_charge FOREIGN KEY (id_charge) REFERENCES Charge(id_charge);
ALTER TABLE Etre_causee ADD CONSTRAINT FK_Etre_causee_id_charge FOREIGN KEY (id_charge) REFERENCES Charge(id_charge);
ALTER TABLE Etre_causee ADD CONSTRAINT FK_Etre_causee_id_commande FOREIGN KEY (id_commande) REFERENCES Commande(id_commande);
