package fr.dl.metier;

import java.util.Date;

public class Charge {

    private int id;
    private String libelle;
    // variabilité : 1 = charge fixe, 2 = charge variable
    private int variabilite;
    private float montant;
    private Date dateContraction;
    private Date datePaiement;

    public Charge() {

    }

    public Charge(int id, String libelle, float montant, Date dateContraction, int variabilite) {
        this.id = id;
        this.libelle = libelle;
        this.montant = montant;
        this.dateContraction = dateContraction;
        this.variabilite = variabilite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public Date getDateContraction() {
        return dateContraction;
    }

    public void setDateContraction(Date dateContraction) {
        this.dateContraction = dateContraction;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getVariabilite() {
        return variabilite;
    }

    public void setVariabilite(int variabilite) {
        this.variabilite = variabilite;
    }
}
