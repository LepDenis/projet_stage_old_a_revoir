package fr.dl.dao;

import fr.dl.metier.Personne;
import fr.dl.metier.Qualite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PersonneDAO {

    private Logger logger = LogManager.getLogger();

    public ArrayList<Personne> getLike(Personne personneSearched) {

        ArrayList<Personne> listePersonnes = new ArrayList<Personne>();
        ResultSet rs = null;

        if (personneSearched.getNom() == null)
            personneSearched.setNom("");
        if (personneSearched.getPrenom() == null)
            personneSearched.setPrenom("");
        if (personneSearched.getTelephone() == null)
            personneSearched.setTelephone("");
        if (personneSearched.getLigneAdresse1() == null)
            personneSearched.setLigneAdresse1("");
        if (personneSearched.getLigneAdresse2() == null)
            personneSearched.setLigneAdresse2("");
        if (personneSearched.getAdresseEmail() == null)
            personneSearched.setAdresseEmail("");

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchpersonne ('" + personneSearched.getNom() + "','" + personneSearched.getPrenom() +
                    "','" + personneSearched.getTelephone() + "','" + personneSearched.getLigneAdresse1() + "','"
                    + personneSearched.getLigneAdresse2() + "'," + personneSearched.getQualite().getId() + ",'" +
                    personneSearched.getAdresseEmail() + "')";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Personne personne = new Personne(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), personneSearched.getQualite(),
                        rs.getString(8));

                listePersonnes.add(personne);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listePersonnes;
    }

    public ArrayList<Personne> getAllByQualite(Qualite qualite) {

        ArrayList<Personne> listePersonnes = new ArrayList<Personne>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchpersonnebyqualite (" + qualite.getId() + ")";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Personne personne = new Personne(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(8));
                listePersonnes.add(personne);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listePersonnes;
    }

    public boolean create(Personne personneCreated) {

        if (personneCreated.getPrenom() == null)
            personneCreated.setPrenom("");
        if (personneCreated.getTelephone() == null)
            personneCreated.setTelephone("");
        if (personneCreated.getLigneAdresse1() == null)
            personneCreated.setLigneAdresse1("");
        if (personneCreated.getLigneAdresse2() == null)
            personneCreated.setLigneAdresse2("");
        if (personneCreated.getAdresseEmail() == null)
            personneCreated.setAdresseEmail("");

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call createpersonne('" + personneCreated.getNom() + "','" + personneCreated.getPrenom() +
                    "','" + personneCreated.getTelephone() + "','" + personneCreated.getLigneAdresse1() + "','" +
                    personneCreated.getLigneAdresse2() + "'," + personneCreated.getQualite().getId() + ",'" + personneCreated.getAdresseEmail() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + personneCreated.getNom() + " " + personneCreated.getPrenom() + " a été créé dans la table personne " +
                    "en tant que " + personneCreated.getQualite().getLibelle() + ".");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Personne personneSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call updatepersonne(" + personneSelected.getId() + ",'" + personneSelected.getNom() +
                    "','" + personneSelected.getPrenom() + "','" + personneSelected.getTelephone() + "','" +
                    personneSelected.getLigneAdresse1() + "','" + personneSelected.getLigneAdresse2() +
                    "'," + personneSelected.getQualite().getId() + ",'" + personneSelected.getAdresseEmail() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement d'id = " + personneSelected.getId() + " a été modifié dans la table personne");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean delete(Personne personneSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call deletepersonne(" + personneSelected.getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + personneSelected.getNom() + " " + personneSelected.getPrenom() +
                    " a été supprimé de la table personne");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public int getMaxId() {

        int maxId = 0;
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();
            String strCmd = "select max(id_personne) from personne";
            rs = stmt.executeQuery(strCmd);
            if (rs.next())
                maxId = rs.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return maxId;
    }
}
