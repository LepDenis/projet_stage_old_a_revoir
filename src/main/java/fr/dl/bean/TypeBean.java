package fr.dl.bean;

import fr.dl.dao.TypeDAO;
import fr.dl.metier.Type;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class TypeBean implements Serializable {

    private ArrayList<Type> types;
    private Type typeSearched;
    private Type typeSelected;
    private Type typeCreated;

    @PostConstruct
    private void initialize() {
        typeSearched = new Type();
        typeSelected = new Type();
        typeCreated = new Type();
        types = new TypeDAO().getAll();
    }

    public void research() {
        TypeDAO dao = new TypeDAO();
        types = dao.getLike(typeSearched);
    }

    public void create() {
        TypeDAO dao = new TypeDAO();
        int i = dao.getMaxId() + 1;
        if (dao.create(typeCreated)) {
            typeCreated.setId(i);
            types.add(typeCreated);
            typeCreated = new Type(i + 1, "");
        }
    }

    public void update() {
        TypeDAO dao = new TypeDAO();
        if (typeSelected != null) {
            int i = types.indexOf(typeSelected);
            if (dao.update(typeSelected)) {
                types.remove(i);
                types.add(i, typeSelected);
            }
        }
    }

    public void delete() {
        TypeDAO dao = new TypeDAO();
        if (typeSelected != null && dao.delete(typeSelected))
            types.remove(typeSelected);
    }

    public ArrayList<Type> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<Type> types) {
        this.types = types;
    }

    public Type getTypeSearched() {
        return typeSearched;
    }

    public void setTypeSearched(Type typeSearched) {
        this.typeSearched = typeSearched;
    }

    public Type getTypeSelected() {
        return typeSelected;
    }

    public void setTypeSelected(Type typeSelected) {
        this.typeSelected = typeSelected;
    }

    public Type getTypeCreated() {
        return typeCreated;
    }

    public void setTypeCreated(Type typeCreated) {
        this.typeCreated = typeCreated;
    }
}
