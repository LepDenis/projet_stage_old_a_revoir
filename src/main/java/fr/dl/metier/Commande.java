package fr.dl.metier;

import java.util.Date;

public class Commande {

    private int id;
    private Date dateCommande;
    private Date dateReception;
    private Personne fournisseur;

    public Commande() {

    }

    public Commande(int id, Date dateCommande, Personne fournisseur) {
        this.id = id;
        this.dateCommande = dateCommande;
        this.fournisseur = fournisseur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Personne getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Personne fournisseur) {
        this.fournisseur = fournisseur;
    }
}
