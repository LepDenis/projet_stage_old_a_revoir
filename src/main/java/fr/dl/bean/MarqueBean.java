package fr.dl.bean;

import fr.dl.dao.MarqueDAO;
import fr.dl.metier.Marque;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class MarqueBean implements Serializable {

    // Déclaration des champs
    private ArrayList<Marque> marques;
    private Marque marqueSearched;
    private Marque marqueSelected;
    private Marque marqueCreated;

    // On crée les objets dont on aura besoin
    // et on initialise la liste de tous les objets
    @PostConstruct
    private void initialize() {
        marqueSearched = new Marque();
        marqueSelected = new Marque();
        marqueCreated = new Marque();
        marques = new MarqueDAO().getAll();
    }

    // Méthode de recherche
    public void research() {
        MarqueDAO dao = new MarqueDAO();
        marques = dao.getLike(marqueSearched);
    }

    // Méthode de création
    public void create() {
        MarqueDAO dao = new MarqueDAO();
        int i = dao.getMaxId() + 1;
        // Si le booléen renvoyé par la classe DAO est
        // à l'état faux, rien ne se passe.
        if (dao.create(marqueCreated)) {
            marqueCreated.setId(i);
            marques.add(marqueCreated);
            // On réinitialise l'objet.
            marqueCreated = new Marque(i + 1, "");
        }
    }

    // Méthode de mise à jour
    public void update() {
        MarqueDAO dao = new MarqueDAO();
        // Ne s'enclenche que si la sélection existe
        if (marqueSelected != null) {
            int i = marques.indexOf(marqueSelected);
            // Si le booléen renvoyé par la classe DAO est
            // à l'état faux, rien ne se passe.
            if (dao.update(marqueSelected)) {
                marques.remove(i);
                // On replace l'objet modifié au même index.
                marques.add(i, marqueSelected);
            }
        }
    }

    // Méthode de suppression
    public void delete() {
        MarqueDAO dao = new MarqueDAO();
        // Si le booléen renvoyé par la classe DAO est
        // à l'état faux, rien ne se passe.
        if (dao.delete(marqueSelected))
            marques.remove(marqueSelected);
    }

    // Accesseurs
    public ArrayList<Marque> getMarques() {
        return marques;
    }

    public void setMarques(ArrayList<Marque> marques) {
        this.marques = marques;
    }

    public Marque getMarqueSearched() {
        return marqueSearched;
    }

    public void setMarqueSearched(Marque marqueSearched) {
        this.marqueSearched = marqueSearched;
    }

    public Marque getMarqueSelected() {
        return marqueSelected;
    }

    public void setMarqueSelected(Marque marqueSelected) {
        this.marqueSelected = marqueSelected;
    }

    public Marque getMarqueCreated() {
        return marqueCreated;
    }

    public void setMarqueCreated(Marque marqueCreated) {
        this.marqueCreated = marqueCreated;
    }
}
