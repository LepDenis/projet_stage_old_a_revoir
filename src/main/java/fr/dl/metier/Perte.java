package fr.dl.metier;

public class Perte {

    private int idPerte;
    private int idPiece;

    public Perte() {

    }

    public Perte(int idPerte, int idPiece) {
        this.idPerte = idPerte;
        this.idPiece = idPiece;
    }

    public int getIdPerte() {
        return idPerte;
    }

    public void setIdPerte(int idPerte) {
        this.idPerte = idPerte;
    }

    public int getIdPiece() {
        return idPiece;
    }

    public void setIdPiece(int idPiece) {
        this.idPiece = idPiece;
    }

}
