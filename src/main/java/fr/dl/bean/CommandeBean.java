package fr.dl.bean;

import fr.dl.metier.Commande;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;

public class CommandeBean implements Serializable {

    private ArrayList<Commande> commandes;
    private Commande commandeSearched;
    private Commande commandeSelected;
    private Commande commandeCreated;

    @PostConstruct
    private void initialize() {

    }

    public void research() {

    }

    public void create() {

    }

    public void update() {

    }

    public void delete() {

    }

    public ArrayList<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(ArrayList<Commande> commandes) {
        this.commandes = commandes;
    }

    public Commande getCommandeSearched() {
        return commandeSearched;
    }

    public void setCommandeSearched(Commande commandeSearched) {
        this.commandeSearched = commandeSearched;
    }

    public Commande getCommandeSelected() {
        return commandeSelected;
    }

    public void setCommandeSelected(Commande commandeSelected) {
        this.commandeSelected = commandeSelected;
    }

    public Commande getCommandeCreated() {
        return commandeCreated;
    }

    public void setCommandeCreated(Commande commandeCreated) {
        this.commandeCreated = commandeCreated;
    }
}
