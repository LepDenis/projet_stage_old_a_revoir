package fr.dl.bean;

import fr.dl.metier.Piece;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class PieceBean implements Serializable {

    private ArrayList<Piece> pieces;
    private Piece pieceSearched;
    private Piece pieceSelected;
    private Piece pieceCreated;

    @PostConstruct
    private void initialize() {

    }

    public void research() {

    }

    public void create() {

    }

    public void update() {

    }

    public void delete () {

    }

    public ArrayList<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(ArrayList<Piece> pieces) {
        this.pieces = pieces;
    }

    public Piece getPieceSearched() {
        return pieceSearched;
    }

    public void setPieceSearched(Piece pieceSearched) {
        this.pieceSearched = pieceSearched;
    }

    public Piece getPieceSelected() {
        return pieceSelected;
    }

    public void setPieceSelected(Piece pieceSelected) {
        this.pieceSelected = pieceSelected;
    }

    public Piece getPieceCreated() {
        return pieceCreated;
    }

    public void setPieceCreated(Piece pieceCreated) {
        this.pieceCreated = pieceCreated;
    }
}
