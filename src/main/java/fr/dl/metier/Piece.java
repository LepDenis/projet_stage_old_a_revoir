package fr.dl.metier;

public class Piece {

    private int id;
    private String modele;
    private int stock;
    private float cout;
    private Marque marque;
    private Type type;

    public Piece() {

    }

    public Piece(int id, String modele, int stock, float cout, Marque marque, Type type) {
        this.id = id;
        this.modele = modele;
        this.stock = stock;
        this.cout = cout;
        this.marque = marque;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getCout() {
        return cout;
    }

    public void setCout(float cout) {
        this.cout = cout;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return modele;
    }
}
