package fr.dl.bean;

import fr.dl.dao.InterventionDAO;
import fr.dl.metier.Intervention;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class InterventionBean implements Serializable {

    private ArrayList<Intervention> interventions;
    private Intervention interventionSearched;
    private Intervention interventionSelected;
    private Intervention interventionCreated;

    @PostConstruct
    private void initialize() {
        interventionSearched = new Intervention();
        interventionSelected = new Intervention();
        interventionCreated = new Intervention();
        interventions = new InterventionDAO().getAll();
    }

    public void research() {

    }

    public void create() {

    }

    public void update() {

    }

    public void delete() {

    }

    public ArrayList<Intervention> getInterventions() {
        return interventions;
    }

    public void setInterventions(ArrayList<Intervention> interventions) {
        this.interventions = interventions;
    }

    public Intervention getInterventionSearched() {
        return interventionSearched;
    }

    public void setInterventionSearched(Intervention interventionSearched) {
        this.interventionSearched = interventionSearched;
    }

    public Intervention getInterventionSelected() {
        return interventionSelected;
    }

    public void setInterventionSelected(Intervention interventionSelected) {
        this.interventionSelected = interventionSelected;
    }

    public Intervention getInterventionCreated() {
        return interventionCreated;
    }

    public void setInterventionCreated(Intervention interventionCreated) {
        this.interventionCreated = interventionCreated;
    }
}
