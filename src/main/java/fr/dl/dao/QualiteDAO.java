package fr.dl.dao;

import fr.dl.metier.Qualite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class QualiteDAO {

    Logger logger = LogManager.getLogger();

    public ArrayList<Qualite> getLike(Qualite qualiteSearched) {

        ArrayList<Qualite> listeQualites = new ArrayList<Qualite>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchqualite ('" + qualiteSearched.getLibelle() + "')";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Qualite qualite = new Qualite(rs.getInt(1), rs.getString(2));
                listeQualites.add(qualite);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return listeQualites;
    }

    public ArrayList<Qualite> getAll() {

        ArrayList<Qualite> listeQualites = new ArrayList<Qualite>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "select * from Qualite";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Qualite qualite = new Qualite(rs.getInt(1), rs.getString(2));
                listeQualites.add(qualite);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listeQualites;
    }

    public boolean create(Qualite qualiteCreated) {

        if (qualiteCreated.getLibelle() == null)
            qualiteCreated.setLibelle("");

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call createqualite ('" + qualiteCreated.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + qualiteCreated.getLibelle() + " a été créé dans la table Qualité.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Qualite qualiteSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call updatequalite (" + qualiteSelected.getId() + ",'" + qualiteSelected.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement d'id = " + qualiteSelected.getId() + " a été modifié dans la table Qualité.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean delete(Qualite qualiteSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call deletequalite (" + qualiteSelected.getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + qualiteSelected.getLibelle() + " a été supprimé de la table Qualité.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public int getMaxId() {
        int maxId = 0;
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();
            String strCmd = "select max(id_qualite) from qualite";
            rs = stmt.executeQuery(strCmd);
            if (rs.next())
                maxId = rs.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return maxId;
    }
}
