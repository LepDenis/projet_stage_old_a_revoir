package fr.dl.bean;

import fr.dl.metier.Perte;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class PerteBean implements Serializable {

    private ArrayList<Perte> pertes;
    private Perte perteSearched;
    private Perte perteSelected;
    private Perte perteCreated;

    @PostConstruct
    private void initialize() {

    }

    public void research() {

    }

    public void create() {

    }

    public void update() {

    }

    public void delete() {

    }

    public ArrayList<Perte> getPertes() {
        return pertes;
    }

    public void setPertes(ArrayList<Perte> pertes) {
        this.pertes = pertes;
    }

    public Perte getPerteSearched() {
        return perteSearched;
    }

    public void setPerteSearched(Perte perteSearched) {
        this.perteSearched = perteSearched;
    }

    public Perte getPerteSelected() {
        return perteSelected;
    }

    public void setPerteSelected(Perte perteSelected) {
        this.perteSelected = perteSelected;
    }

    public Perte getPerteCreated() {
        return perteCreated;
    }

    public void setPerteCreated(Perte perteCreated) {
        this.perteCreated = perteCreated;
    }
}
