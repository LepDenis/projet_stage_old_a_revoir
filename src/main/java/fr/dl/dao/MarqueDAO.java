package fr.dl.dao;

import fr.dl.metier.Marque;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class MarqueDAO {

    private Logger logger = LogManager.getLogger();

    public ArrayList<Marque> getAll() {

        ArrayList<Marque> listeMarques = new ArrayList<Marque>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "select * from Marque";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Marque marque = new Marque(rs.getInt(1), rs.getString(2));
                listeMarques.add(marque);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return listeMarques;
    }

    public ArrayList<Marque> getLike(Marque marqueSearched) {

        ArrayList<Marque> listeMarques = new ArrayList<Marque>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchmarque ('" + marqueSearched.getLibelle() + "')";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Marque marque = new Marque(rs.getInt(1), rs.getString(2));
                listeMarques.add(marque);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        return listeMarques;
    }

    public boolean create(Marque marqueCreated) {

        if (marqueCreated.getLibelle() == null)
            marqueCreated.setLibelle("");

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call createmarque ('" + marqueCreated.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + marqueCreated.getLibelle() + " a été créé dans la table Marque.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Marque marqueSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call updatemarque (" + marqueSelected.getId() + ",'" + marqueSelected.getLibelle() + "')";

            stmt.execute(strCmd);

            logger.info("L'enregistrement d'id = " + marqueSelected.getId() + " a été modifié dans la table Marque.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean delete(Marque marqueSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call deletemarque (" + marqueSelected.getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + marqueSelected.getLibelle() + " a été supprimé de la table Marque.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public int getMaxId() {
        int maxId = 0;
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();
            String strCmd = "select max(id_marque) from marque";
            rs = stmt.executeQuery(strCmd);
            if (rs.next())
                maxId = rs.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return maxId;
    }
}
