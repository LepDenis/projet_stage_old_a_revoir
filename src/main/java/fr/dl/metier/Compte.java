package fr.dl.metier;

public class Compte {

    private int id;
    private String login;
    private String password;
    private int niveauAutorisation;
    private Personne proprietaire;

    public Compte() {

    }

    public Compte(int id, String login, String password, int niveauAutorisation, Personne proprietaire) {
        this.id = id;
        this.password = password;
        this.niveauAutorisation = niveauAutorisation;
        this.proprietaire = proprietaire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNiveauAutorisation() {
        return niveauAutorisation;
    }

    public void setNiveauAutorisation(int niveauAutorisation) {
        this.niveauAutorisation = niveauAutorisation;
    }

    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

}
