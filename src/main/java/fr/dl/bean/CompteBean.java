package fr.dl.bean;

import fr.dl.metier.Compte;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;

public class CompteBean implements Serializable {

    private ArrayList<Compte> comptes;
    private Compte compteSearched;
    private Compte compteSelected;
    private Compte compteCreated;

    @PostConstruct
    private void initialize() {

    }

    public void research() {

    }

    public void create() {

    }

    public void update() {

    }

    public void delete() {

    }

    public ArrayList<Compte> getComptes() {
        return comptes;
    }

    public void setComptes(ArrayList<Compte> comptes) {
        this.comptes = comptes;
    }

    public Compte getCompteSearched() {
        return compteSearched;
    }

    public void setCompteSearched(Compte compteSearched) {
        this.compteSearched = compteSearched;
    }

    public Compte getCompteSelected() {
        return compteSelected;
    }

    public void setCompteSelected(Compte compteSelected) {
        this.compteSelected = compteSelected;
    }

    public Compte getCompteCreated() {
        return compteCreated;
    }

    public void setCompteCreated(Compte compteCreated) {
        this.compteCreated = compteCreated;
    }
}
