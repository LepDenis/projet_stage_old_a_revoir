package fr.dl.metier;

public class Appareil {

    // Déclaration des champs
    private int id;
    private String modele;
    private Type type;
    private Marque marque;

    // Constructeurs
    public Appareil() {
    }

    public Appareil(int id, String modele, Type type, Marque marque) {
        this.id = id;
        this.modele = modele;
        this.type = type;
        this.marque = marque;
    }

    // Accesseurs
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    @Override
    public String toString() {
        return modele;
    }
}
