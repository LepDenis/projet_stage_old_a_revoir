package fr.dl.metier;

import java.util.Date;

public class Intervention {

    private int id;
    // lieu : 1 = au magasin, 2 = à domicile
    private int lieu;
    private Date dateDebut;
    private Date dateFin;
    private Personne client;
    private Personne technicien;
    private float sommeDue;
    private Appareil appareil;
    private Panne panne;
    private Statut statut;

    public Intervention() {

    }

    public Intervention(int id, int lieu, Date dateDebut, Date dateFin, Personne client, Personne technicien, float sommeDue, Appareil appareil,
                        Panne panne, Statut statut) {
        this.id = id;
        this.lieu = lieu;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.client = client;
        this.technicien = technicien;
        this.sommeDue = sommeDue;
        this.appareil = appareil;
        this.panne = panne;
        this.statut = statut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLieu() {
        return lieu;
    }

    public void setLieu(int lieu) {
        this.lieu = lieu;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Personne getClient() {
        return client;
    }

    public void setClient(Personne client) {
        this.client = client;
    }

    public Personne getTechnicien() {
        return technicien;
    }

    public void setTechnicien(Personne technicien) {
        this.technicien = technicien;
    }

    public float getSommeDue() {
        return sommeDue;
    }

    public void setSommeDue(float sommeDue) {
        this.sommeDue = sommeDue;
    }

    public Appareil getAppareil() {
        return appareil;
    }

    public void setAppareil(Appareil appareil) {
        this.appareil = appareil;
    }

    public Panne getPanne() {
        return panne;
    }

    public void setPanne(Panne panne) {
        this.panne = panne;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }
}
