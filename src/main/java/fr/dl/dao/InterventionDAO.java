package fr.dl.dao;

import fr.dl.metier.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class InterventionDAO {

    private Logger logger = LogManager.getLogger();

    public ArrayList<Intervention> getAll() {

        ArrayList<Intervention> listeInterventions = new ArrayList<Intervention>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call QBEintervention('','','',0,null,null,null,null,0,0,'','','')";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Intervention intervention = new Intervention(rs.getInt(2), rs.getInt(3),
                        rs.getDate(4), rs.getDate(5),
                        new Personne(rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9),
                                rs.getString(10), rs.getString(11), rs.getString(12)),
                        new Personne(rs.getInt(13), rs.getString(14), rs.getString(15), rs.getString(16),
                                rs.getString(17), rs.getString(18), rs.getString(19)),
                        rs.getFloat(1),
                        new Appareil(rs.getInt(20), rs.getString(21), new Type(rs.getInt(22), rs.getString(23)),
                                new Marque(rs.getInt(24), rs.getString(25))),
                        new Panne(rs.getInt(26), rs.getString(27)),
                        new Statut(rs.getInt(28), rs.getString(29)));
                listeInterventions.add(intervention);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listeInterventions;
    }

    public ArrayList<Intervention> getLike(Intervention interventionSearched) {

        ArrayList<Intervention> listeInterventions = new ArrayList<Intervention>();
        ResultSet rs = null;


        return listeInterventions;
    }

    public boolean create(Intervention interventionCreated) {

        return true;
    }

    public boolean update(Intervention interventionSelected) {

        return true;
    }

    public boolean delete(Intervention interventionSelected) {

        return true;
    }

}
