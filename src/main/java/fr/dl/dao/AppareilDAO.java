package fr.dl.dao;

import fr.dl.metier.Appareil;
import fr.dl.metier.Marque;
import fr.dl.metier.Type;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class AppareilDAO {

    private Logger logger = LogManager.getLogger();

    public ArrayList<Appareil> getAll() {

        ArrayList<Appareil> listeAppareils = new ArrayList<Appareil>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchappareil (null,null,null)";
            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Appareil appareil = new Appareil(rs.getInt(1), rs.getString(2),
                        new Type(rs.getInt(3), rs.getString(4)),
                        new Marque(rs.getInt(5), rs.getString(6)));
                listeAppareils.add(appareil);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listeAppareils;
    }

    public ArrayList<Appareil> getLike(Appareil appareilSearched) {

        ArrayList<Appareil> listeAppareils = new ArrayList<Appareil>();
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call searchappareil ('" + appareilSearched.getModele() + "',"
                    + appareilSearched.getType().getId() + ","
                    + appareilSearched.getMarque().getId() + ")";

            rs = stmt.executeQuery(strCmd);

            while (rs.next()) {
                Appareil appareil = new Appareil(rs.getInt(1), rs.getString(2),
                        new Type(rs.getInt(3), rs.getString(4)),
                        new Marque(rs.getInt(5), rs.getString(6)));
                listeAppareils.add(appareil);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return listeAppareils;
    }

    public boolean create(Appareil appareilCreated) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call createappareil('" + appareilCreated.getModele()
                    + "'," + appareilCreated.getType().getId() +
                    "," + appareilCreated.getMarque().getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + appareilCreated.getModele()
                    + " a été créé dans la table appareil.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Appareil appareilSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call updateappareil(" + appareilSelected.getId() + ",'"
                    + appareilSelected.getModele() + "'," + appareilSelected.getType().getId()
                    + "," + appareilSelected.getMarque().getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement d'id = " + appareilSelected.getId()
                    + " a été modifié dans la table appareil.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean delete(Appareil appareilSelected) {

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();

            String strCmd = "call deletepersonne(" + appareilSelected.getId() + ")";

            stmt.execute(strCmd);

            logger.info("L'enregistrement " + appareilSelected.getModele()
                    + " a été supprimé de la table appareil.");

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }

    public int getMaxId() {

        int maxId = 0;
        ResultSet rs = null;

        try {
            Statement stmt = Connect.getInstance().getConnection().createStatement();
            String strCmd = "select max(id_appareil) from appareil";
            rs = stmt.executeQuery(strCmd);
            if (rs.next())
                maxId = rs.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return maxId;
    }

}
