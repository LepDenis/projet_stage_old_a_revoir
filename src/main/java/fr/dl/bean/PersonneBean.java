package fr.dl.bean;

import fr.dl.dao.PersonneDAO;
import fr.dl.metier.Personne;
import fr.dl.metier.Qualite;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class PersonneBean implements Serializable {

    private ArrayList<Personne> personnes;
    private Personne personneSearched;
    private Personne personneSelected;
    private Personne personneCreated;

    @PostConstruct
    private void initialize() {
        personneSearched = new Personne();
        personneSelected = new Personne();
        personneCreated = new Personne();
    }

    public String clients() {
        Qualite client = new Qualite(1, "client");
        personneSearched.setQualite(client);
        personneCreated.setQualite(client);
        return rediriger();
    }

    public String techniciens() {
        Qualite technicien = new Qualite(2, "technicien");
        personneSearched.setQualite(technicien);
        personneCreated.setQualite(technicien);
        return rediriger();
    }

    public String fournisseurs() {
        Qualite fournisseur = new Qualite(3, "fournisseur");
        personneSearched.setQualite(fournisseur);
        personneCreated.setQualite(fournisseur);
        return rediriger();
    }

    private String rediriger() {
        personnes = new PersonneDAO().getAllByQualite(personneSearched.getQualite());
        return "personnes.xhtml?faces-redirect=true";
    }

    public void research() {
        PersonneDAO dao = new PersonneDAO();
        personnes = dao.getLike(personneSearched);
    }

    public void create() {
        PersonneDAO dao = new PersonneDAO();
        int i = dao.getMaxId() + 1;
        if (dao.create(personneCreated)) {
            personneCreated.setId(i);
            personnes.add(personneCreated);
            personneCreated = new Personne((i + 1), "", "", "", "", "",
                    personneCreated.getQualite(), "");
        }
    }

    public void update() {
        PersonneDAO dao = new PersonneDAO();
        if (personneSelected != null) {
            personneSelected.setQualite(personneSearched.getQualite());
            int i = personnes.indexOf(personneSelected);
            if (dao.update(personneSelected)) {
                personnes.remove(i);
                personnes.add(i, personneSelected);
            }
        }
    }

    public void delete() {
        PersonneDAO dao = new PersonneDAO();
        if (personneSelected != null) {
            personneSelected.setQualite(personneSearched.getQualite());
            if (dao.delete(personneSelected))
                personnes.remove(personneSelected);
        }
    }

    public ArrayList<Personne> getPersonnes() {
        return personnes;
    }

    public void setPersonnes(ArrayList<Personne> personnes) {
        this.personnes = personnes;
    }

    public Personne getPersonneSearched() {
        return personneSearched;
    }

    public void setPersonneSearched(Personne personneSearched) {
        this.personneSearched = personneSearched;
    }

    public Personne getPersonneSelected() {
        return personneSelected;
    }

    public void setPersonneSelected(Personne personneSelected) {
        this.personneSelected = personneSelected;
    }

    public Personne getPersonneCreated() {
        return personneCreated;
    }

    public void setPersonneCreated(Personne personneCreated) {
        this.personneCreated = personneCreated;
    }

}
